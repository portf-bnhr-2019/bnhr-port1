require('offline-plugin/runtime').install();

import './index.html';
import './me.html';
import './blog.html';
import './blog-detail.html';
import './contact.html';
import './404.html';
import './menu.html';
import './menu1.html';
import './work.html';
import './work-detail.html';
import './index.scss';
import './scripts/script.js';

import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';