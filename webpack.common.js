const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/index.js',
    vendor: './src/vendor.js'
  },
  module: {
    rules: [
      {
        test: /\.txt$/,
        use: 'raw-loader'
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'images/',
              publicPath: '/images/'
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/',
              publicPath: 'fonts/'
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new WriteFilePlugin(),
    new CopyPlugin([
      {
        from: path.resolve(__dirname, 'src', 'robots.txt'),
        to: path.resolve(__dirname, 'dist', 'robots.txt')
      }
    ]),
    new HtmlWebpackPlugin({
      title: 'heru-home-page',
      filename: 'index.html',
      template: './src/index.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'heru-me-page',
      filename: 'me.html',
      template: './src/me.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'heru-blog-page',
      filename: 'blog.html',
      template: './src/blog.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'heru-blog-detail-page',
      filename: 'blog-detail.html',
      template: './src/blog-detail.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'heru-contact-page',
      filename: 'contact.html',
      template: './src/contact.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'heru-404-page',
      filename: '404.html',
      template: './src/404.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'menu',
      filename: 'menu.html',
      template: './src/menu.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'menu1',
      filename: 'menu1.html',
      template: './src/menu1.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'heru-work',
      filename: 'work.html',
      template: './src/work.html',
      inject: 'head'
    }),
    new HtmlWebpackPlugin({
      title: 'heru-work-detail',
      filename: 'work-detail.html',
      template: './src/work-detail.html',
      inject: 'head'
    }),
    new PreloadWebpackPlugin({
      rel: 'preload',
      as(entry) {
        if (/\.(woff|woff2|ttf|otf)$/.test(entry)) return 'font';
      },
      fileWhitelist: [/\.(woff|woff2|ttf|otf)$/],
      include: 'allAssets'
    }),
    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'defer'
    })
  ],
  externals: {
    $: 'jquery',
    jquery: 'jQuery',
    'window.$': 'jquery'
  }
};
